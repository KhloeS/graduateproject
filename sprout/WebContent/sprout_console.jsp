<%@page import="java.nio.file.Path"%>
<%@page import="sprout.Execution"%>
<%@page import="sprout.SpringfieldPrompt"%>
<%@page import ="java.io.*" %>
<%@page import = "java.util.*" %>
<%@ page language="java" contentType="text/html; charset=euc-kr"
    pageEncoding="euc-kr" trimDirectiveWhitespaces="true"%>
<html>
<head><title>콘솔실행결과</title></head>
<body>
실행중입니다...
<%
String code = (String)request.getSession().getAttribute("sproutcode");
String current = getServletContext().getRealPath("\\");
SpringfieldPrompt sp = new SpringfieldPrompt(code);
ArrayList<String> list = sp.getcodelist();
String s;
Process p = null;
Process pr = null;
File file = new File(current+"\\새싹파일\\sprout.ss");
if(!file.exists()){
	file.createNewFile();
}
PrintWriter pw = new PrintWriter(file);
pw.write(code);
pw.flush();
pw.close();
ProcessBuilder pb =new ProcessBuilder();
if(current != System.getProperty("user.dir")){
	current = current+"\\console";
	pb.directory(new File(current));//현재 디렉토리 설정.
}
boolean complete = true;
Execution p_check = new Execution();
ArrayList<String[]>console_command = p_check.consolecommand();
for(String[] com:console_command) {
	p = pb.command(com).start();
	p.waitFor();
	if(!p_check.isExited(p.exitValue())){
		out.println("새싹 파일이 실행되지 않습니다.<br/>");
		BufferedReader stdErr   = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		out.println("<font color = \"red\">");
		while((s = stdErr.readLine())!=null){
			out.println(s+"<br />");
		}
		out.println("</font>");
		complete = false;
		break;
	}
	p.destroy();
}
if(complete) {
	current = current + "\\src";
	pb.directory(new File(current));
	BufferedReader compile   = new BufferedReader(new InputStreamReader(p.getInputStream()));
	p.waitFor();
	if(p.exitValue() == 0) {
		/*pr = pb.command("java","sprout").start();
		BufferedWriter stdin = new BufferedWriter(new OutputStreamWriter(pr.getOutputStream()));
		BufferedReader stdOut = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		BufferedReader stdErr = new BufferedReader(new InputStreamReader(pr.getErrorStream()));*/
		if(sp.hasInput()){
			//out.println("input이 있습니다.");
			ArrayList<String> prompt = sp.promptlist();
			request.getSession().setAttribute("prompt", prompt);
			//out.println("<body onload = \"promWindowOpen()\">");
			response.sendRedirect("prompt.jsp");
		}
		else {
			response.sendRedirect("consoleresult.jsp");
		}
		/*while((s= stdOut.readLine())!=null) {
			out.println(s+"<br />");
		}
		if(stdErr.ready()){
			out.println("<font color = \"red\">");
			out.println("실행 오류가 생겼습니다.<br />");//오류처리
			while((s = stdErr.readLine())!=null) {
				out.println(s +"<br />");
			}
			out.println("</font>");
		}
		stdOut.close();*/
		/*response.sendRedirect("consoleresult.jsp");
		out.println("<p>" + "변수를 입력하세요. 변수를 입력할 때 SPACE키를 사용하여 변수를 구별하여야 합니다.<br/>입력 변수가 필요한 경우에만 입력하세요." + "</p>");
		out.println("<form action=\"consoleresult.jsp\" method=\"POST\">");
		out.println("<input type=\"text\" name=\"spinput\">");
		out.println("<input type = \"submit\" value = \"실행하기\"/><br>");
		out.println("</form>");*/		
	}
	else {
		out.println("<font color = \"red\">");
		out.println("실행 오류가 생겼습니다.<br />");//오류처리
		while((s = compile.readLine())!=null) {
			out.println(s +"<br />");
		}
		out.println("</font>");
	}
	
}
%>
</body>

</html>