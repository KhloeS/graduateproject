@echo off
cd console
java -jar ../compile/sprout-compiler_console.jar sprout.ss
javac -Xlint src/*.java
cd src
copy *.class .. > nul
cd ..
java sprout