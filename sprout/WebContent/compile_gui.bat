@echo off
cd gui
java -jar ../compile/package-converter.jar sprout.ss
java -jar ../compile/sprout-compiler.jar sprout.ss
copy sprout.ss src > nul
javac -Xlint src/*.java
cd src
copy *.class .. > nul
del sprout.ss > nul
cd ..
type nul > manifest.txt
jar -cvmf manifest.txt sprout.jar *.class > nul
keytool -genkey -alias sprout -keyalg RSA -keystore sprout.keystore -storetype JCEKS -validity 36500  -dname "cn=sprout, ou=sprout, o=sprout, c=KR" -keypass 123456 -storepass 123456 > nul
jarsigner -keystore sprout.keystore -storetype JCEKS -storepass 123456 -keypass 123456 sprout.jar sprout > nul
cd ..
copy guiresult.jsp gui > nul