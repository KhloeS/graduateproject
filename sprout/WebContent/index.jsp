<%@page import="org.apache.tomcat.util.net.SecureNio2Channel.ApplicationBufferHandler"%>
<%@ page language="java" contentType="text/html; charset=euc-kr"
	pageEncoding="euc-kr" trimDirectiveWhitespaces="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr"/>
<link rel=stylesheet href="lib/codemirror.css">
<script src="lib/codemirror.js"></script>
<script src="addon/edit/closebrackets.js"></script>
<script src="addon/edit/matchbrackets.js"></script>
<script src="sproutformat.js"></script>
<script src="FileSaver.js"></script>
<script src="encoding-indexes.js"></script>
<script src="encoding.js"></script>
<style type="text/css" id=style>
      .CodeMirror {border: 1px solid black;}
</style>
<title>새싹을 실행해 봅시다!</title>
</head>
<%
%>
<body>
<form action="sprout_execute.jsp" method="POST" target="result">
<textarea rows="30" style="width:100%;width:80%" id ="sprouttextfield" name = "sprouttextfield">시작(문자열[] 값) {
  출력("안녕, 세상!");
}</textarea>
<br />
실행 방식
        <label><input type="radio" name="compileop" value="0" checked>콘솔 실행</label>
        <label><input type="radio" name="compileop" value="1">GUI 실행</label>
        <input type="submit" value="실행" />
        <input type = "button" value="초기화" onclick = "initialize()"/>
        <input type = "button"  value = "지우기" onclick="codeerase()"/>
        <input type = "button"  value = "저장" onclick="codesave()"/>
        <input type = "button" value = "불러오기" onclick = "codeload()"/>
        <!--input id = "uploadfile" type="file" accept ='text/plain' name ="uploadfile" style="display:none;" onchange='openFile(event)'/-->
</form>
<form action = "sprout_file.jsp" name = "fileform">
<!--form id = "fileupload"-->
 <input id = "uploadfile" type="file" accept ='text/plain' name ="uploadfile" style="display:none;" onchange='openFile(event)'/>
 <!--input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;"tabindex="-1"/-->
</form>
<br />
실행 결과창
<iframe src="" name = "result" style="width:100%"></iframe>
<script>
  var editor = CodeMirror.fromTextArea(document.getElementById('sprouttextfield'), {
  /*lineNumbers: true,
	theme: "eclipse",
	electricChars: true,
	autoCloseBrackets: true,
  matchBrackets: true,
  tabsize: 4,
  mode: "text/x-csrc"*/
  lineNumbers: true,
  matchBrackets: true,
  autoCloseBrackets: true,
  tabsize: 4,
  mode: "text/x-sprout"
  });
</script>
<script charset="euc-kr">
var fileRequest;
function BrowserSupport() { // check browser support
	try {
		fileRequest = new XMLHttpRequest();
	} catch(e) {
		try {
			fileRequest = new ActiveXObject("Msxm12.XMLHTTP");
		} catch(e) {
			try {
				fileRequest = new ActiveXObject("Microsoft.XMLHTTP");
			}catch(e) {
				alert("Your browser broke!");
				return false;
			}
		}
	}
}

function codeerase() {
	editor.setValue("");
	editor.clearHistory();
}
function initialize() {
	editor.setValue("시작(문자열[] 값){\n\t출력(\"안녕,세상!\");\n}");
}
function codesave() {
	var code = editor.getValue();
	var blob = new Blob([code], {
	    type: "text/plain;charset=euc-kr;"
	});
	saveAs(blob, "sprout.ss");
}
function codeload() {
	if(BrowserSupport()!= false){
		$("#uploadfile").trigger('click');
		document.getElementById('fileform').target = '_self';
		document.forms['fileform'].submit();
	}
}
var openFile = function(event) {
    /*var input = event.target;
    var reader = new FileReader();
    reader.onload = function(){
    	var text = reader.result;
    	editor.setValue(text);
    };
    reader.readAsText(input.files[0],"euc-kr");*/
    /*var form = new FormData(document.getElementById('fileupload'));
    $.ajax({dataType : 'json',
        url : '/sprout_file.jsp',
        data : form,
        type : 'text',
        processData: false, 
        contentType:false,
        type: 'post',
        success : function(data) {
         
        },
        error : function(data) {
         
        }
    });*/
    document.getElementById("uploadfile").submit();
};
</script>
</body>
</html>