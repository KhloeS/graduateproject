<%@page import ="java.io.*" %>
<%@page import = "java.util.*" %>
<%@page import = "sprout.SpringfieldPrompt" %>
<%@ page language="java" contentType="text/html; charset=euc-kr"
    pageEncoding="euc-kr" trimDirectiveWhitespaces="true"%>
<html>
<head><title>GUI실행결과</title></head>
<body>
실행중입니다...
<%
String code = (String)request.getSession().getAttribute("sproutcode");
String current = getServletContext().getRealPath("\\");
String s;
File file = new File(current+"\\gui\\sprout.ss");
if(!file.exists()){
	file.createNewFile();
}
PrintWriter pw = new PrintWriter(file);
pw.write(code);
pw.flush();
pw.close();
String[] commands = {"cmd","/c","compile_gui.bat"};
ProcessBuilder pb =new ProcessBuilder(commands);
if(current != System.getProperty("user.dir")){
	pb.directory(new File(current));
}
Process p = pb.start();
p.waitFor();
BufferedReader stdOut   = new BufferedReader(new InputStreamReader(p.getInputStream()));
BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
while ((s =   stdOut.readLine()) != null) System.out.println(s);
while ((s = stdError.readLine()) != null) System.err.println(s);
System.out.println("Exit Code: " + p.exitValue());
if(p.exitValue()==0){
	//out.println("실행파일 생성 완료!");
	response.sendRedirect("gui/guiresult.jsp");
}
%>
</body>
</html>