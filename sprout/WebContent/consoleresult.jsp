<%@page import="java.lang.ProcessBuilder.Redirect"%>
<%@page import="sprout.*"%>
<%@page import="java.nio.file.Path"%>
<%@page import ="java.io.*" %>
<%@page import = "java.util.*" %>
<%@ page language="java" contentType="text/html; charset=euc-kr"
    pageEncoding="euc-kr" trimDirectiveWhitespaces="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>콘솔실행결과</title></head>
<body>
<body>
<%
String current = getServletContext().getRealPath("\\");
ProcessBuilder pb =new ProcessBuilder();
Process p = null;
if(current != System.getProperty("user.dir")){
	//current = current+"\\console\\src";
	current = current+"\\console";
	pb.directory(new File(current));//현재 디렉토리 설정.
}
String s;
p = pb.command("java","sprout").start();
BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
BufferedReader stdOut   = new BufferedReader(new InputStreamReader(p.getInputStream()));
BufferedReader stdErr   = new BufferedReader(new InputStreamReader(p.getErrorStream()));
String code = (String)request.getSession().getAttribute("sproutcode");
SpringfieldPrompt sp = new SpringfieldPrompt(code);
if(sp.hasInput()) {
	ArrayList<String> prompt = (ArrayList<String>)request.getSession().getAttribute("prompt");
	for(int i = 0; i < prompt.size();++i) {
		String value = new String(request.getParameter("input"+i).toString().getBytes("iso-8859-1"),"euc-kr");
		writer.write(value);
		writer.newLine();
	}
	writer.close();
	p.waitFor();
	if(p.exitValue() == 0) {
		while((s= stdOut.readLine())!=null) {
			for(String pr:prompt){
				if(pr!=null){
					s = s.replaceFirst(pr, "");
				}
			}
			out.println(s+"<br />");
		}
	}
}
else {
	if(p.exitValue() == 0) {
		while((s= stdOut.readLine())!=null) {
			out.println(s+"<br />");
		}
	}
}
if(stdErr.ready()){
	out.println("<font color = \"red\">");
	out.println("실행 오류가 생겼습니다.<br />");//오류처리
	while((s = stdErr.readLine())!=null) {
		out.println(s +"<br />");
	}
	out.println("</font>");
}
stdOut.close();

/*p.waitFor();
out.println("<p>"+"실행을 완료되었습니다!"+"</p>");
out.println("<h2>"+"실행결과"+"</h2>");
while((s= stdOut.readLine())!=null) {
	out.println(s+"<br />");
}
if(stdErr.ready()){
	out.println("<font color = \"red\">");
	out.println("실행 오류가 생겼습니다.<br />");//오류처리
	while((s = stdErr.readLine())!=null) {
		out.println(s +"<br />");
	}
	out.println("</font>");
}
stdOut.close();*/
/*ProcessBuilder pb =new ProcessBuilder();
Process p = null;
if(current != System.getProperty("user.dir")){
	current = current+"\\console\\src";
	pb.directory(new File(current));//현재 디렉토리 설정.
}
SpringfieldPrompt sp = new SpringfieldPrompt(context.getRealPath("\\")+"\\console\\새싹파일\\sprout.ss");
if(sp.isNoInput()) {
	out.println("<p>"+"실행을 완료되었습니다!"+"</p>");
	out.println("<h2>"+"실행결과"+"</h2>");
	String input = request.getParameter("spinput");
	String[] inputs = input.split(" ");
	p = pb.command("java","sprout").start();
	BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
	BufferedReader stdOut   = new BufferedReader(new InputStreamReader(p.getInputStream()));
	BufferedReader stdErr   = new BufferedReader(new InputStreamReader(p.getErrorStream()));
	if(input != null) {
		if(inputs!= null) {
			for (String i: inputs){
				writer.write(i);
			}
		} else {
			writer.write(input);
			for(int j =0; j <input.length();++j){
			}
		}
		writer.newLine();
	}
	p.waitFor();
	while((s= stdOut.readLine())!=null) {
		out.println(s+"<br />");
	}
	if(stdErr.ready()){
		out.println("<font color = \"red\">");
		out.println("실행 오류가 생겼습니다.<br />");//오류처리
		while((s = stdErr.readLine())!=null) {
			out.println(s +"<br />");
		}
		out.println("</font>");
	}
	stdOut.close();
}
else {
	out.println("넘김을 받음");
}*/
%>
</body>
</html>