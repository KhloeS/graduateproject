package sprout;
import java.io.*;
public class EchoInputStream extends InputStream {
	   private InputStream iStream;
	   private OutputStream echoDest;
	   public EchoInputStream(InputStream iStream, OutputStream echoDest) {
	      this.iStream = iStream;
	      this.echoDest = echoDest;
	   }

	   @Override
	   public int read() throws IOException {
	      int readResult = iStream.read();
	      echoDest.write(readResult);
	      return readResult;
	   }
}