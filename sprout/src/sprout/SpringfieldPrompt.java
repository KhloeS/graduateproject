package sprout;

import java.io.*;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpringfieldPrompt {
	private String filename;
	private Scanner sc;
	private String input_regex = "^.*�Է�\\(.*\\);$";//�Է� promptã�¤���...
	private ArrayList<String> codelist;
	public SpringfieldPrompt(String contents) {
		String[] content = contents.split("\r\n");
		codelist = new ArrayList<String>(Arrays.asList(content));
	}
	public ArrayList<String> getcodelist() {
		return codelist;
	}
	public boolean hasInput() {
		boolean flag = false;
		for(String content:codelist) {
			if(content.matches(input_regex)) {
				flag = true;
			}
		}
		return flag;
	}
	public ArrayList<String> promptlist() {
		String previous;
		ArrayList<String> prompt = new ArrayList<String>();
		CopyOnWriteArrayList<String> copycode = new CopyOnWriteArrayList<String>(codelist);
		Iterator<String> iter = copycode.iterator();
		while(iter.hasNext()) {
			String s = iter.next();
			if((!s.matches("^.*�Է�\\(.*\\);$")&&(!s.matches("^.*���\\(.*\\);$")))) {
				copycode.remove(s);
			}
		}
		Pattern p2 = Pattern.compile("�Է�\\((.*?)\\);$");
		if(copycode.get(0).matches(input_regex)){
			Matcher m = p2.matcher(copycode.get(0));
			if(m.find()) {
				prompt.add(null);
			}
		}
		Pattern p1 =Pattern.compile("���\\((.*?)\\);$");
		for(int i = 1; i < copycode.size();++i) {
			if(copycode.get(i).matches(input_regex)){
				previous = copycode.get(i-1);
				Matcher m = p1.matcher(previous);
				if(m.find()) {
					String Message = m.group(1);
					Message = escchange(Message);
					if(Message.contains("\",\"")) {
						String[] messages = Message.split("\"");
						prompt.add(formatting(messages));
					} else {
						prompt.add(Message.replaceAll("^\"|\"$", ""));
					}
				}
				else{
					prompt.add("");
				}
			}
		}
		return prompt;
	}
	private String escchange(String source) {
		if(source.contains("\\��")) {
			source = source.replace("\\��", "");
		} else if(source.contains("\\��")) {
			source = source.replace("\\��", "\\t");
		} else if(source.contains("\\��")) {
			source = source.replace("\\��", "\\b");
		} else if(source.contains("\\÷")) {
			source = source.replace("\\÷", "\\r");
		} else if (source.contains("\\��")) {
			source = source.replace("\\��", "\\f");
		}
		return source;
	}
	private String formatting(String[] source) {
		ArrayList<String> formatbuf = new ArrayList<String>();
		Collections.addAll(formatbuf, source);
		formatbuf.remove("");
		formatbuf.remove(",");
		String formatstring = formatbuf.remove(0);
		int idx = formatstring.indexOf("%",0);
		String replace;
		while(!formatbuf.isEmpty()) {
			if(idx < 0) {
				break;
			}
			if(isformatstring(formatstring.substring(idx,2))) {
				replace = formatbuf.remove(0);
				formatstring = formatstring.replaceFirst(formatstring.substring(idx,2), replace);
				idx = formatstring.indexOf("%",idx+replace.length());
			} else if(isformatstring(formatstring.substring(idx,3))) {
				replace = formatbuf.remove(0);
				formatstring=formatstring.replace(formatstring.substring(idx,3), replace);
				idx = formatstring.indexOf("%",idx+replace.length());
				
			} else if(isformatstring(formatstring.substring(idx,4))) {
				replace = formatbuf.remove(0);
				formatstring=formatstring.replace(formatstring.substring(idx,4), replace);
				idx = formatstring.indexOf("%",idx+replace.length());
			} else if(isformatstring(formatstring.substring(idx,5))) {
				replace = formatbuf.remove(0);
				formatstring=formatstring.replace(formatstring.substring(idx,5), replace);
				idx = formatstring.indexOf("%",idx+replace.length());
			} else if(isformatstring(formatstring.substring(idx,6))) {
				replace = formatbuf.remove(0);
				formatstring=formatstring.replace(formatstring.substring(idx,6), replace);
				idx = formatstring.indexOf("%",idx+replace.length());
			} else if(isformatstring(formatstring.substring(idx,7))) {
				replace = formatbuf.remove(0);
				formatstring=formatstring.replace(formatstring.substring(idx,7), replace);
				idx = formatstring.indexOf("%",idx+replace.length());
			} 
			else {
				idx++;
			}
		}
		return formatstring;
	}
	private boolean isformatstring(String source){
		String pattern1 = "^%(��|��|��|��|��)$";
		String pattern2 = "^%_(8|16)$";
		String pattern3 = "^%-?[0-9]*(.[0-9]+)?��$";
		if(source.matches(pattern1)||source.matches(pattern2)||source.matches(pattern3)) {
			return true;
		}
		else {
			return false;
		}
	}
}
